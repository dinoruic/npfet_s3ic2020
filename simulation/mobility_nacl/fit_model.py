import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
from scipy.optimize import curve_fit

print("NaCl Mobility Model:")
print(50*"-")
print()

conductivity_filenames = [
    "data/bianchi_concentration_vs_conductivity.csv",
    "data/currie_concentration_vs_conductivity.csv",
    "data/goldsack_concentration_vs_conductivity.csv",
    "data/monica_concentration_vs_conductivity.csv"
]

transfer_filenames = [
    "data/currie_concentration_vs_Na_transfer.csv",
    "data/haynes_concentration_vs_Na_transfer.csv",
    "data/monica_concentration_vs_Na_transfer.csv",
    "data/panopoulos_concentration_vs_Na_transfer.csv",
    "data/schonert_concentration_vs_Na_transfer.csv",
    "data/smits_concentration_vs_Na_transfer.csv"
]

# Read files and flatten array
conductivities = np.concatenate(
        [
            np.array(read_csv(f, header=None))
            for f in conductivity_filenames
        ]
    )

na_transfers = np.concatenate(
        [
            np.array(read_csv(f, header=None))
            for f in transfer_filenames
        ]
    )


##################################################
### Na transfer number fit
##################################################

def na_transfer_fit(c, cref, A, t0):
    return A * np.exp( - (c / cref)**0.5 ) + t0

bounds = (1e-3,5)
popt_t, pcov_t = curve_fit(na_transfer_fit,
        na_transfers[:,0], na_transfers[:,1],
        bounds=bounds)

indent = 3*" "
print(indent + "Na transfer number function:")
print()
print(2*indent + "t_Na(c) = A * exp(- (c / cref)**0.5) + t0")
print(2*indent + 3*" " + "cref = {} M".format(popt_t[0]))
print(2*indent + 3*" " + "A    = {}".format(popt_t[1]))
print(2*indent + 3*" " + "t0   = {}".format(popt_t[2]))
print()
print()


##################################################
### Conductivity fit
##################################################
#conductivity at infinite dilution
def conductivity_fit(c, c_ref, cond0, A):
    return A*c + cond0/(1 + (c/c_ref)**(1/3))

bounds = ([1e0, 1e0, -1e2], [1e3, 1e4, 1e2])
popt_cond, pcov_cond = curve_fit(conductivity_fit,
        conductivities[:,0],
        conductivities[:,1],
        bounds=bounds)


print(indent + "Conductivity Function:")
print()
print(2*indent +
    "cond(c) = A * c + cond0/(1 + (c/c_ref)**(1/3))" )
print(2*indent + 2*" " + "c_ref = {} M".format(popt_cond[0]))
print(2*indent + 2*" " + "cond0 = {} C cm^2 / V s mol".format(popt_cond[1]))
print(2*indent + 2*" " + "A     = {} C cm^2 / V s mol M ".format(popt_cond[2]))
print()
print()


print(indent + "Na conductivity:")
print()
print(2*indent + "cond_Na(c) = cond(c) * t_Na(c)")
print()
print()

print(indent + "Cl conductivity:")
print()
print(2*indent + "cond_Cl(c) = cond(c) * (1 - t_Na(c))")
print()
print()



##################################################
## Plotting
##################################################
# compute conductivities of Na and Cl
conductivities_na = np.array(list(
        map(
            lambda x: [x[0], na_transfer_fit(x[0], *popt_t) * x[1]],
            conductivities.tolist()
        )
    ))

conductivities_cl = np.array(list(
        map(
            lambda x: [x[0], (1 - na_transfer_fit(x[0], *popt_t)) * x[1]],
            conductivities.tolist()
        )
    ))



# Plot Transfer Number Fit
ax = plt.subplot(1,2,1)
ax.set_title("Transfer number data and fit")
ax.set_ylabel("Transfer Number for Na")
ax.set_xlabel("Concentration [M]")
legend=[]

ax.plot(na_transfers[:,0], na_transfers[:,1], marker="x", linestyle="")
legend.append("Experimental Data")

cs = np.linspace(0, 5, 1000)
ax.plot(cs, na_transfer_fit(cs, *popt_t))
legend.append("t_Na(c) = {:.5f} * exp( - (c / {:.5f}M)**0.5) + {:.5f}".format(
    popt_t[1],popt_t[0],popt_t[2]))

ax.legend(legend)

# Conductivities
ax = plt.subplot(1,2,2)
ax.set_title("Conductivities")
ax.set_xlabel("Concentration [M]")
ax.set_ylabel("Conductivity [C cm^2 / V s mol ]")
ax.set_xscale('log')
legend = []

plt.plot(conductivities[:,0], conductivities[:,1], marker="x", linestyle="")
legend.append("total conductivity")

cs = np.logspace(-3.5, 1, 100)
ax.plot(cs, conductivity_fit(cs, *popt_cond))
legend.append("cond(c) = {:.3f}*c + {:.3f}/(1 + (c/{:.3f}M)**(1/3))".format(
    popt_cond[2], popt_cond[1], popt_cond[0]))

ax.plot(conductivities_na[:,0], conductivities_na[:,1], marker="x", linestyle="")
legend.append("Na conductivity")

ax.plot(cs, conductivity_fit(cs, *popt_cond) * na_transfer_fit(cs, *popt_t))
legend.append("cond_Na = cond_(c) * t_Na(c)")

ax.plot(conductivities_cl[:,0], conductivities_cl[:,1], marker="x", linestyle="")
legend.append("Cl conductivity")

ax.plot(cs,
        conductivity_fit(cs, *popt_cond) * (1 - na_transfer_fit(cs, *popt_t)))
legend.append("cond_Cl = cond_(c) * (1 - t_Na(c))")


ax.legend(legend)

plt.show()
