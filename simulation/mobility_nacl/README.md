# Mobility Model for Electrolyte

The mobility of the ions in the electrolyte is strongly dependent on the
concentration. Since the concentration varies strongly in a nanopore, we need to
include this effect. To this end we use a Thomas-Caughey mobility model but we
don't use the field but the concentration in the numerator. This will give us a
reasonable fit that can be used in the Nernst-Planck equation.

Mobilities can be computed from the conductivities multiplied by the transfer
number which indicates how much of the total current is carried by the ion
species.

Conductivities are in units of `S*cm^2/mol`. Concentrations are in Molar 
`M = mol/L`.


# Data for NaCl
The data is in the csv files and it is taken from the following sources

* 1986 Panopoulos JSC: Transference Numbers of Sodium Chloride in Concentrated
  Aqueous Solutions and Chloride Conductances in Several Concentrated
  Electrolyte Solutions
* 1960 Currie JPC: Transference numbers for concentrated aqueous sodium chloride
  solutions, and the ionic conductances for potassium and sodium chlorides
* 1989 Bianchi JSC: The Conductivity of Concentrated Aqueous Mixtures of NaCl
  and MgCI2 at 25C
* W. M. Haynes, David R. Lide, Thomas J. Bruno CRC Handbook of Chemistry and
  Physics 2016-2017
* 1976 Goldsack CJC: Solvation effects on the conductivity of concentrated
  electrolyte solutions
* 1966 Smits&Duyvis JACS: Transport Numbers of Concentrated Sodium Chloride
  Solutions at 25C
* 2014 Schonert JSC: Transference Numbers in the H2O + NaCl + MgCl2
* 1979 Monica EA: Transference numbers in concentrated sodium chloride solutions
