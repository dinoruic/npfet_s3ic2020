/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  dev                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      physicalProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// UNITS: [kg m  s K mol A cd]
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// relative permittivities
epsilon_oxide   3.9; 
epsilon_semi    11.68; 
epsilon_fluid   80.1;  //water at 20C
epsilon_molecule 2;


// Temperature
T   300; //[K]

// bulk salt ion concentration
c_bulk  1e0; //[mol/m^3 = 1e-3 M]

//bulk H concentratin
pH_bulk  7; //[1]

// intrinsic carrier concentration
ni   1.45e16; //[1/m^3]

// electron mobility
nmobility   100e-4; //[m^2/V/s = A*s^2/kg]

// hole mobility
pmobility   100e-4; //[m^2/V/s = A*s^2/kg]


//Thomas-Caughey mobility model
//Electron majority
//nmobility_min   74.5e-4; // [m^2/V/s]
nmobility_min   37.25e-4; // [m^2/V/s]
//nmobility_max   1430e-4; // [m^2/V/s]
nmobility_max   715e-4; // [m^2/V/s]
nN_ref           8.6e22; // [1/m^3]
nalpha             0.77; // [1]
//velocity saturation
nvsat               1e5; // [m/s] 
nbeta                 2; // [1] 

//Hole minority
pmobility_min   122.3e-4; // [m^2/V/s]
pmobility_max     480e-4; // [m^2/V/s]
pN_ref            1.4e23; // [1/m^3]
palpha              0.70; // [1]
//velocity saturation
pvsat              0.9e5; // [m/s] 
pbeta                  1; // [1] 


// fixed surface charge on the oxide-fluid interface 
// (negative means negatively charged states)
//sfdensity   -1e17;  //[1/m^2]
sfdensity   0;  //[1/m^2]

// fixed volume fixed oxide charges in the gate oxide
// (negative means negatively charged states)
//fixedoxidedensity  1e22;  //[1/m^3]
fixedoxidedensity  0;  //[1/m^3]

//Oxide trap parameters.
//If only pMOS or nMOS is considered, the lifetimes are irrelevant.
Ntrap    1e15; //[1/m^2]
n1_trap  5e21; //[1/m^3]
p1_trap  5e21; //[1/m^3]
ntau_trap  1e-6; //[s]
ptau_trap  1e-6; //[s]

// Silanol surface charge density
//Nsil    4.6e18; //[1/m^2]
Nsil    0.48e18; //[1/m^2]
//Nsil    0; //[1/m^2]

// double protonation: SiOH2+ <-> SiOH + H+
pKd -1.9;  //both sites
// single protonation: SiOH <-> SiO- + H+
pKs1 4.5;  //first site
//pKs2 8.5;  //second site
pKs2 8;  //second site
// percentage occupied by first state with pKs1 (second is the rest)
//silanol_site_occupation 0.19; //[0,1]
silanol_site_occupation 0.5; //[0,1]

//////////////////////////////////////////////////
// Ion mobilities
//////////////////////////////////////////////////
//cation transfer number function
t_cref  1719.74936355463; //[mol/m^3 = 1e-3 M] 
t_A     0.05283400141854182; //[1]
t0      0.3434894382279226; //[1]

//ion conductivity function
cond_cref  15268.974353205152; //[mol/m^3 = 1e-3 M] 
cond0     0.01276771248173786;   // [C m^2 / V s mol]
cond_A    -5.13728755360843e-07;  // [C m^5 / V s mol^2]


//H
hmobility 50e-8; // [m^2/V/S] 
//OH
ohmobility 50e-8; // [m^2/V/S]

//maximum concentration when size effects are considered in the fluid
kconc_max       5e3;   //[mol/m^3 = 1e-3 M] 
aconc_max       5e3;   //[mol/m^3 = 1e-3 M] 
hconc_max      50e3;   //[mol/m^3 = 1e-3 M] 
ohconc_max     50e3;   //[mol/m^3 = 1e-3 M] 


//////////////////////////////////////////////////// 
// fluid flow
//////////////////////////////////////////////////// 
mass_density 1e3; //[kg/m^3]
//kinematic viscosity
viscosity 1e-6;  // [m^2/s]

//////////////////////////////////////////////////// 
// Molecule
//////////////////////////////////////////////////// 
molecule_elementary_charges 10; //[e]



// ************************************************************************* //
