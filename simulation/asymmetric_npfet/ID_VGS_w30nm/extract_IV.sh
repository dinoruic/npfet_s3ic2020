#! /usr/bin/env bash

DIR="run"
OUTFILE="IV.asc"

t="1"
currentfile="terminalCurrentsDict"
Icontact="source"

potentialfile="pot"
Vcontact="cis"

rm -f $OUTFILE

currentfiles=$(find $DIR -type f -name $currentfile)
rundirs=$(dirname $(dirname $currentfiles))

for dir in $rundirs
do
    Id=$(cat $dir/$t/$currentfile \
            | grep $Icontact \
            | awk '{print $NF}' \
            | sed "s/;//g") 
    V=$(cat $dir/$t/$potentialfile \
            | grep -A 4 $Vcontact  \
            | grep value \
            | awk '{print $NF}' \
            | sed "s/;//g") 

    echo "$V $Id" >> $OUTFILE
done

sort $OUTFILE -o $OUTFILE
