#! /usr/bin/env bash

template="npfet_3d_template"

declare -a moleculearray=("40e-9" "30e-9" "15e-9" "0e-9" "-15e-9" "-30e-9" "-40e-9" "-50e-9")

vd="0.10"
declare -a vgarray=("0.58")
vtrans="0.5"


run_dir=run
rm -rf $run_dir

for mol in "${moleculearray[@]}" 
do
    dir_name="${run_dir}/molecule_${mol}nm"
    echo "Create new directory $dir_name"
    mkdir -p $dir_name

    # switch to directory and create ID-VGS run
    for vg in "${vgarray[@]}"
    do
        instance="${dir_name}/VG_${vg}V"
        echo "Create $instance instance"
        cp -r $template $instance
        
        #change drain bias
        sed -i "s/VDRAIN/${vd}/g" $instance/0.orig/semi/nimref
        sed -i "s/VDRAIN/${vd}/g" $instance/0.orig/semi/pimref

        #Change gate bias
        sed -i "s/VCIS/${vg}/g" $instance/0.orig/pot
        sed -i "s/VTRANS/${vtrans}/g" $instance/0.orig/pot

        #Change molecule position
        sed -i "s/MOLECULE/${mol}/g" $instance/system/topoSetDict
    done

    echo
done
