cp ../ID_Pos_reference/run/molecule_0e-9nm/VG_0.58V/1/pot run/molecule_-40e-9nm/VG_0.58V/1/pot_ref
cp ../ID_Pos_reference/run/molecule_0e-9nm/VG_0.58V/1/pot run/molecule_-15e-9nm/VG_0.58V/1/pot_ref
cp ../ID_Pos_reference/run/molecule_0e-9nm/VG_0.58V/1/pot run/molecule_0e-9nm/VG_0.58V/1/pot_ref
cp ../ID_Pos_reference/run/molecule_0e-9nm/VG_0.58V/1/pot run/molecule_30e-9nm/VG_0.58V/1/pot_ref

sed -i "s/pot/pot_ref/g" run/molecule_-40e-9nm/VG_0.58V/1/pot_ref
sed -i "s/pot/pot_ref/g" run/molecule_-15e-9nm/VG_0.58V/1/pot_ref
sed -i "s/pot/pot_ref/g" run/molecule_0e-9nm/VG_0.58V/1/pot_ref
sed -i "s/pot/pot_ref/g" run/molecule_30e-9nm/VG_0.58V/1/pot_ref
