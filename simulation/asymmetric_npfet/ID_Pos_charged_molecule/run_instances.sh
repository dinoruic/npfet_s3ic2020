#! /usr/bin/env bash

run_dir=run

CWD=$(pwd)

for vd in $(ls $run_dir)
do
    for vg in $(ls "${run_dir}/${vd}") 
    do
        case_dir="${run_dir}/${vd}/${vg}"
        echo "Starting simulation of ${case_dir}"
        (cd ${case_dir} && exec ./Allrun &)
    done
done
