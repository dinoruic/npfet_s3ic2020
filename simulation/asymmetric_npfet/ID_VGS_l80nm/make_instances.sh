#! /usr/bin/env bash

template="npfet_3d_template"

declare -a vdarray=("0.10")
declare -a vgarray=("0.20" "0.30" "0.40" "0.50" "0.60" "0.70" "0.80" "0.90" "1.00")

vd="0.10"


run_dir=run
rm -rf $run_dir

for vd in "${vdarray[@]}" 
do
    dir_name="${run_dir}/VDS_${vd}V"
    echo "Create new directory $dir_name"
    mkdir -p $dir_name

    # switch to directory and create ID-VGS run
    for vg in "${vgarray[@]}"
    do
        instance="${dir_name}/VG_${vg}V"
        echo "Create $instance instance"
        cp -r $template $instance
        
        #change drain bias
        sed -i "s/VDRAIN/${vd}/g" $instance/0.orig/semi/nimref
        sed -i "s/VDRAIN/${vd}/g" $instance/0.orig/semi/pimref

        #Change gate bias
        sed -i "s/VCIS/${vg}/g" $instance/0.orig/pot
        sed -i "s/VTRANS/${vg}/g" $instance/0.orig/pot

        #Change molecule position
        sed -i "s/MOLECULE/${mol}/g" $instance/system/topoSetDict
    done

    echo
done
