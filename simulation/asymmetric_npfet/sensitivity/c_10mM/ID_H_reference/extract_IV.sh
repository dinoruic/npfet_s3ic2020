#! /usr/bin/env bash

DIR="run"
OUTFILE="IV.asc"

t="1"
currentfile="terminalCurrentsDict"
Icontact="source"

hconcfile="0.orig/fluid/hconc"

rm -f $OUTFILE

currentfiles=$(find $DIR -type f -name $currentfile)
rundirs=$(dirname $(dirname $currentfiles))


for dir in $rundirs
do
    Id=$(cat $dir/$t/$currentfile \
            | grep $Icontact \
            | awk '{print $NF}' \
            | sed "s/;//g") 
    H=$(cat $dir/$hconcfile \
            | grep -A 4 cis \
            | grep value \
            | awk '{print $3}' \
            | sed "s/;//g") 

    echo "$H $Id" >> $OUTFILE
done

sort -g $OUTFILE -o $OUTFILE
