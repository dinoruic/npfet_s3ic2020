#! /usr/bin/env bash

template="npfet_3d_template"

declare -a Harray=("1e-4" "1e-3" "1e-2" "1e-1" "1e0")

vd="0.10"
declare -a vgarray=("0.58")
vtrans="0.5"
mol=("-30e-9")

run_dir=run
rm -rf $run_dir

for H in "${Harray[@]}" 
do
    dir_name="${run_dir}/H_${H}mM"
    echo "Create new directory $dir_name"
    mkdir -p $dir_name

    # switch to directory and create ID-VGS run
    for vg in "${vgarray[@]}"
    do
        instance="${dir_name}/VG_${vg}V"
        echo "Create $instance instance"
        cp -r $template $instance
        
        #change drain bias
        sed -i "s/VDRAIN/${vd}/g" $instance/0.orig/semi/nimref
        sed -i "s/VDRAIN/${vd}/g" $instance/0.orig/semi/pimref

        #Change gate bias
        sed -i "s/VCIS/${vg}/g" $instance/0.orig/pot
        sed -i "s/VTRANS/${vtrans}/g" $instance/0.orig/pot

        #Change molecule position
        sed -i "s/MOLECULE/${mol}/g" $instance/system/topoSetDict

        #Change H concentration
        sed -i "s/HCONC/${H}/g" $instance/0.orig/fluid/hconc
        sed -i "s/HCONC/${H}/g" $instance/0.orig/fluid/ohconc
    done

    echo
done
