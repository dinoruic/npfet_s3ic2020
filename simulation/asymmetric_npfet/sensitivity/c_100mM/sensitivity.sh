refdir=$(ls | grep *reference)
moldir=$(ls | grep *molecule)

(cd $refdir && ./extract_IV.sh)
(cd $moldir && ./extract_IV.sh)

paste $refdir/IV.asc $moldir/IV.asc \
    | awk '{if ($1 == $3) print -log($1*1e-3)/log(10.0), sqrt((100*($2 - $4)/$2)^2)}' \
    > sensitivity.asc
