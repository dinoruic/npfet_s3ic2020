/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  dev                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    dpot
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       1e-14;
        relTol          1e-10;
    }

    pot
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       1e-14;
        relTol          1e-10;
    }

    "fundamental.*"
    {
        solver          PCG;
        preconditioner  DIC;
        tolerance       1e-14;
        relTol          1e-10;
    }

}

//Global relaxation scheme for the set of equations
relaxation
{
    //Poisson equation underrelaxation factor
    poissonRelax 1;
    nernstPlanckRelax 1;

    //Each of these equations is at least individually iterated until this
    //tolerance is reached
    poissonTol       0.1;
    nernstPlanckTol  2;
    ddTol            1e-3;

    //If the Poisson-DD-Nernst-Planck system reaches this convergence, the
    //Navier-Stokes is first calculated
    preNSTol        1e-5;

    //least amount of tolerance for the Navier-Stokes
    nsTol           1e-4;
    nsMinIter       1;
    nsMaxIter       500;

    //All equations are relaxed iteratively until this tolerance is reached
    globalTol        1e-5;
    maxIter          500;
}

// ************************************************************************* //
