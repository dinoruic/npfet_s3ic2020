#! /usr/bin/env bash

DIR="run"
OUTFILE="IV.asc"

t="1"
currentfile="terminalCurrentsDict"
Icontact="source"

topoSetDict="system/topoSetDict"

rm -f $OUTFILE

currentfiles=$(find $run -type f -name $currentfile)
rundirs=$(dirname $(dirname $currentfiles))

for dir in $rundirs
do
    Id=$(cat $dir/$t/$currentfile \
            | grep $Icontact \
            | awk '{print $NF}' \
            | sed "s/;//g") 
    pos=$(cat $dir/$topoSetDict \
            | grep origin \
            | awk '{print $3}')

    echo "$pos $Id" >> $OUTFILE
done

sort -g $OUTFILE -o $OUTFILE
