# Design and Modeling of a Nanopore Transistor

Single-molecule biosensors based on ion current sensing through nanopores have
found a wide array of applications including next-generation DNA sequencing
devices.
They are also promising candidates to unlock the multi-billion-dollar markets
for proteomics and point-of-care life sciences devices.
The nanopore FET (NPFET) has been proposed by Xie et al. (Nat. Nanotechnol.,
vol. 7, pp. 119-125, 2012) as an alternative design to detect ion current
modulations and molecular translocations.
It can solve multiple challenges holding back conventional
nanopores by offering high signal currents, high bandwidth, scalable
manufacturing, dense integration, and parallel sensing.

The NPFET is a transistor wrapped around a nanopore connecting two electrolyte
reservoirs (Fig. 1).
It can detect analyte molecules passing through the nanopore as a change in the
source-drain current by:

- a potential perturbation due to the the blocking of ion current in the
  nanopore and
- direct electrostatic gating by the molecule charge.

Optimizing the NPFET design for these mechanisms requires an in-depth
understanding of the physics of deeply scaled silicon devices, the nature of
electrolyte flow and ion currents in nanopores, and surface charges and their
pH-dependence.

Since there are no robust and calibrated solvers for these types of biosensor
systems, we set out to solve this problem for the wider research community by
building the open-source npfetFOAM solver which unifies the physics of
semiconductors and flowing electrolytes in one simulation tool.

The npfetFOAM solver consists of a 3D self-consistent finite-volume-based
solution of the Poisson equation for the electric potential, the drift-diffusion
equation for the semiconductor, as well as the Nernst-Planck and Navier-Stokes
equations for the electrolyte.
We calibrate the semiconductor and surface state models using experimental data
from electrolytically gated FinFETs (Fig. 2).
The concentration-dependent ion mobilities are obtained by a fit to
experimental bulk values from literature.

We compute the effects of the translocation of a charged molecule (Fig. 3) and
extract insights about drain current signal profiles (Fig. 4) as well as
sensitivities for various buffer conditions.
With currents in the micro-ampere-range and signals that can easily
exceed 20%, the NPFET is a promising candidate to enable next-generation
biosensing fully integrated in a scalable CMOS process.

The results of the npfetFOAM solver are directly informing the design and
integration of the NPFET at imec.
