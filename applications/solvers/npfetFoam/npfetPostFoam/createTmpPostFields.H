    volScalarField semiPot
    (
        IOobject
        (
            "semiPot",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        toRegionField(semiRegion, semiCellAddr, pot)
    );

    volScalarField fluidPot
    (
        IOobject
        (
            "fluidPot",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        toRegionField(fluidRegion, fluidCellAddr, pot)
    );


    surfaceVectorField ncurrent
    (
        IOobject
        (
            "ncurrent",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        semiRegion,
        dimensionedVector
        (
            "ncurrent",
            dimensionSet(0,-2,0,0,0,1,0),
            vector(0.0,0.0,0.0)
        )
    );


    surfaceVectorField pcurrent
    (
        IOobject
        (
            "pcurrent",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        semiRegion,
        dimensionedVector
        (
            "pcurrent",
            dimensionSet(0,-2,0,0,0,1,0),
            vector(0.0,0.0,0.0)
        )
    );

    surfaceVectorField kcurrent
    (
        IOobject
        (
            "kcurrent",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion,
        dimensionedVector
        (
            "kcurrent",
            dimensionSet(0,-2,0,0,0,1,0),
            vector(0.0,0.0,0.0)
        )
    );


    surfaceVectorField acurrent
    (
        IOobject
        (
            "acurrent",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion,
        dimensionedVector
        (
            "acurrent",
            dimensionSet(0,-2,0,0,0,1,0),
            vector(0.0,0.0,0.0)
        )
    );


    // TODO aimref/kimref/himref/ohimref should be computed from concentrations
    volScalarField kimref
    (
        IOobject
        (
            "kimref",
            runTime.timeName(),
            fluidRegion,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion
    );

    volScalarField aimref
    (
        IOobject
        (
            "aimref",
            runTime.timeName(),
            fluidRegion,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion
    );

    volScalarField himref
    (
        IOobject
        (
            "himref",
            runTime.timeName(),
            fluidRegion,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion
    );

    volScalarField ohimref
    (
        IOobject
        (
            "ohimref",
            runTime.timeName(),
            fluidRegion,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion
    );

    surfaceScalarField kmobility
    (
        IOobject
        (
            "kmobility",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        cationMobility
        (
            kconc, cond_A, cond0, cond_cref, t_A, t0, t_cref
        ),
        calculatedFvPatchScalarField::typeName
    );

    surfaceScalarField amobility
    (
        IOobject
        (
            "amobility",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        anionMobility
        (
            aconc, cond_A, cond0, cond_cref, t_A, t0, t_cref
        ),
        calculatedFvPatchScalarField::typeName
    );

    const dimensionedScalar hmobility_value
    (
        "hmobility",
        dimMobility,
        physicalProperties
    );

    surfaceScalarField hmobility
    (
        IOobject
        (
            "hmobility",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion,
        hmobility_value,
        calculatedFvPatchScalarField::typeName
    );

    const dimensionedScalar ohmobility_value
    (
        "ohmobility",
        dimMobility,
        physicalProperties
    );


    surfaceScalarField ohmobility
    (
        IOobject
        (
            "ohmobility",
            runTime.timeName(),
            fluidRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        fluidRegion,
        ohmobility_value,
        calculatedFvPatchScalarField::typeName
    );

    const dimensionedScalar nmobility_min
    (
        "nmobility_min",
        dimMobility,
        physicalProperties
    );

    const dimensionedScalar nmobility_max
    (
        "nmobility_max",
        dimMobility,
        physicalProperties
    );

    const dimensionedScalar nN_ref
    (
        "nN_ref",
        dimless / dimVol,
        physicalProperties
    );

    const dimensionedScalar nalpha
    (
        "nalpha",
        dimless,
        physicalProperties
    );

    const dimensionedScalar nvsat
    (
        "nvsat",
        dimLength/dimTime,
        physicalProperties
    );

    const dimensionedScalar nbeta
    (
        "nbeta",
        dimless,
        physicalProperties
    );


    const dimensionedScalar pmobility_min
    (
        "pmobility_min",
        dimMobility,
        physicalProperties
    );

    const dimensionedScalar pmobility_max
    (
        "pmobility_max",
        dimMobility,
        physicalProperties
    );

    const dimensionedScalar pN_ref
    (
        "pN_ref",
        dimless / dimVol,
        physicalProperties
    );

    const dimensionedScalar palpha
    (
        "palpha",
        dimless,
        physicalProperties
    );

    const dimensionedScalar pvsat
    (
        "pvsat",
        dimLength/dimTime,
        physicalProperties
    );

    const dimensionedScalar pbeta
    (
        "pbeta",
        dimless,
        physicalProperties
    );




    // Thomas-Caughey model:
    // doping dependent low-field mobilities
    surfaceScalarField nmobility0
    (
        IOobject
        (
            "nmobility0",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        TCmobility0(nmobility_min, nmobility_max, nN_ref, nalpha, totalDoping),
        calculatedFvPatchScalarField::typeName
    );

    surfaceScalarField nmobility
    (
        IOobject
        (
            "nmobility",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        nmobility0
    );


    // Thomas-Caughey model:
    // doping dependent low-field mobilities
    surfaceScalarField pmobility0
    (
        IOobject
        (
            "pmobility0",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        TCmobility0(pmobility_min, pmobility_max, pN_ref, palpha, totalDoping),
        calculatedFvPatchScalarField::typeName
    );

    surfaceScalarField pmobility
    (
        IOobject
        (
            "pmobility",
            runTime.timeName(),
            semiRegion,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        pmobility0
    );


