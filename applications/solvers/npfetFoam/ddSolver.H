    //  Matrix access.
    //  Addressing:
    //      lowerAddr = owner
    //      upperAddr = neighbour
    //
    //  Only in the Scharfetter-Gummel discretized case will the matrix by asymmetric but we will
    //      always assume it so it is easier to switch between the naive
    //      discretization and Scharfetter-Gummel


    //temporary delta field
    tmp<volScalarField> tdimref
    (
        new volScalarField
        (
            IOobject
            (
                "dimref",
                imref.instance(),
                imref.mesh(),
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            semiRegion,
            dimensionedScalar("dimref", imref.dimensions(), 0),
            fixedValueFvPatchField<scalar>::typeName
        )
    );
    volScalarField& dimref = tdimref.ref();


    tmp<fvScalarMatrix> tMatrix
    (
        new fvScalarMatrix
        (
            dimref,
            //TODO fix dimensions, they are not important for the correctness of
            //the equations but its good to be consistent with the physics
            dimVol / pow2(dimLength)  / dimVol * imref.dimensions()
        )
    );
    fvScalarMatrix& matrix = tMatrix.ref();

    scalarField& d = matrix.diag();
    scalarField& u = matrix.upper();
    scalarField& l = matrix.lower();


    scalarField& s = matrix.source();

    //////////////////////////////////////////////////
    // Laplace Operator
    //////////////////////////////////////////////////
    forAll (semiRegion.owner(), iFace)
    {
        label own = semiRegion.owner()[iFace];
        label nei = semiRegion.neighbour()[iFace];

        // owner, neighbour coupling
        scalar diagonal_on;
        scalar offdiagonal_on;

        // neighbhour, owner coupling
        scalar diagonal_no;
        scalar offdiagonal_no;

        if ( dd_scharfetter_gummel )
        {
            const scalar grid = semiRegion.magSf()[iFace]
                                * semiRegion.deltaCoeffs()[iFace];

            const scalar b_n =
                bernoulli(z*(semiPot[nei]-semiPot[own])/VT.value());
            const scalar b_o =
                bernoulli(z*(semiPot[own]-semiPot[nei])/VT.value());

            const scalar b_on = b_n * density[nei] - b_o * density[own];


            // owner contribution
            // derivative w.r.t. imref[own]
            diagonal_on =
                grid
                * (
                    - mobility[iFace] *  b_o * ddensity_dimref[own]
                    + dmobility_dimref_o[iFace] * b_on
                );

            // owner contribution
            // derivative w.r.t. imref[nei]
            offdiagonal_on =
                grid
                * (
                    mobility[iFace] * b_n * ddensity_dimref[nei]
                    + dmobility_dimref_n[iFace] * b_on
                );

            // neighbour contribution
            // derivative w.r.t. imref[nei]
            diagonal_no = - offdiagonal_on;

            // neighbour contribution
            // derivative w.r.t. imref[own]
            offdiagonal_no = - diagonal_on;

        }
        else
        {
            // owner contribution
            // derivative w.r.t. imref[own]
            diagonal_on =
                semiRegion.magSf()[iFace] * semiRegion.deltaCoeffs()[iFace]
                * (
                    - mobility[iFace]
                        * (density[own] + density[nei])/2.0
                    + mobility[iFace]
                        * ddensity_dimref[own] / 2.0
                        * (imref[nei] - imref[own])
                    + dmobility_dimref_o[iFace]
                        * (density[own] + density[nei])/2.0
                        * (imref[nei] - imref[own])
                  );

            // owner contribution
            // derivative w.r.t. imref[nei]
            offdiagonal_on =
                semiRegion.magSf()[iFace] * semiRegion.deltaCoeffs()[iFace]
                * (
                    + mobility[iFace]
                        * (density[own] + density[nei])/2.0
                    + mobility[iFace]
                        * ddensity_dimref[nei] / 2.0
                        * (imref[nei] - imref[own])
                    + dmobility_dimref_n[iFace]
                        * (density[own] + density[nei])/2.0
                        * (imref[nei] - imref[own])
                );

            // neighbour contribution
            // derivative w.r.t. imref[nei]
            diagonal_no =
                semiRegion.magSf()[iFace] * semiRegion.deltaCoeffs()[iFace]
                * (
                    - mobility[iFace]
                        * (density[nei] + density[own])/2.0
                    + mobility[iFace]
                        * ddensity_dimref[nei] / 2.0
                        * (imref[own] - imref[nei])
                    + dmobility_dimref_n[iFace]
                        * (density[own] + density[nei])/2.0
                        * (imref[own] - imref[nei])
                );

            // neighbour contribution
            // derivative w.r.t. imref[own]
            offdiagonal_no =
                semiRegion.magSf()[iFace] *  semiRegion.deltaCoeffs()[iFace]
                * (
                    + mobility[iFace]
                        * (density[nei] + density[own])/2.0
                    + mobility[iFace]
                        * ddensity_dimref[own] / 2.0
                        * (imref[own] - imref[nei])
                    + dmobility_dimref_o[iFace]
                        * (density[own] + density[nei])/2.0
                        * (imref[own] - imref[nei])
                );


        }

        // flux out of owner
        d[own] += diagonal_on;
        u[iFace] += offdiagonal_on;

        // flux into neighbour
        d[nei] += diagonal_no;
        l[iFace] += offdiagonal_no;


    }


    //////////////////////////////////////////////////
    // RHS of Newton method
    //////////////////////////////////////////////////

    forAll (semiRegion.owner(), iFace)
    {
        label own = semiRegion.owner()[iFace];
        label nei = semiRegion.neighbour()[iFace];

        // semiRegion.Sf() points from owner to neighbour

        //current from owner to neighbour
        double curr_on;
        //current from neighbour to owner
        double curr_no;

        if (dd_scharfetter_gummel)
        {
            curr_on =
                semiRegion.magSf()[iFace] * semiRegion.deltaCoeffs()[iFace]
                * mobility[iFace]
                * (   bernoulli(z*(semiPot[nei]-semiPot[own])/VT.value())
                    * density[nei]
                    - bernoulli(z*(semiPot[own]-semiPot[nei])/VT.value())
                    * density[own] );

            curr_no = -curr_on;
        }
        else
        {
            curr_on =
                semiRegion.magSf()[iFace] *  semiRegion.deltaCoeffs()[iFace]
                * mobility[iFace]
                * (density[own] + density[nei])/2.0
                * (imref[nei] - imref[own]);

            curr_no = - curr_on;
        }

        //flux out of owner
        s[own] -=  curr_on;

        //flux into neighbour
        s[nei] -=  curr_no;

    }

    //Apply boundary conditions
    const fvPatchList& patches = semiRegion.boundary();
    forAll( patches, iPatch )
    {
        const fvPatch& patch = patches[iPatch];
        const fvPatchField<scalar>& pf = imref.boundaryField()[iPatch];
        if( pf.fixesValue() )
        {
            matrix.setValues
            (
                patch.faceCells(),
                pf - pf.patchInternalField()
            );
        }
    }



    //solve
    imref.storePrevIter();
    matrix.solve();
    tMatrix.clear();

    //update
    imref += dimref;
    tdimref.clear();

    //underrelax
    imref = max(min((imref - imref.prevIter()),VT),-VT) + imref.prevIter();
