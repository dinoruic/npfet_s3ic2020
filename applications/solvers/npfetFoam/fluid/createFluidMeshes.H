    const wordList fluidNames(rp["fluid"]);

    PtrList<fvMesh> fluidRegions(fluidNames.size());

    forAll(fluidNames, i)
    {
        Info<< "Create fluid mesh for region " << fluidNames[i]
            << " for time = " << runTime.timeName() << nl << endl;

        fluidRegions.set
        (
            i,
            new fvMesh
            (
                IOobject
                (
                    fluidNames[i],
                    runTime.timeName(),
                    runTime,
                    IOobject::MUST_READ
                )
            )
        );
    }

    // This is temporary. We'll allow for arbitrary numbers of semi regions in
    // the future
    if (fluidNames.size() != 1)
    {
        FatalErrorInFunction
            << "There can only be one fluid region but there are: "
            << fluidNames.size()
            << exit(FatalError);
    }

    fvMesh& fluidRegion = fluidRegions[0];

    IOList<label> fluidCellAddr
    (
        IOobject
        (
            "polyMesh/cellRegionAddressing",
            runTime.constant(),
            fluidRegion,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

