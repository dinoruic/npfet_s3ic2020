// Read global relaxation control
const dictionary& relaxationDict
    = allRegions.solutionDict().subDict("relaxation");

const scalar poissonRelax =
    relaxationDict.lookupOrDefault("poissonRelax", 1.0);
const scalar nernstPlanckRelax =
    relaxationDict.lookupOrDefault("nernstPlanckRelax", 1.0);

const scalar poissonTol =
    relaxationDict.lookupOrDefault("poissonTol", 1e-3);
const scalar nernstPlanckTol =
    relaxationDict.lookupOrDefault("nernstPlanckTol", 1e-3);
const scalar ddTol =
    relaxationDict.lookupOrDefault("ddTol", 1e-3);
const scalar preNSTol =
    relaxationDict.lookupOrDefault("preNSTol", 1e-5);
const scalar nsTol =
    relaxationDict.lookupOrDefault("nsTol", 1e-3);
const int nsMinIter =
    relaxationDict.lookupOrDefault("nsMinIter", 10);
const int nsMaxIter =
    relaxationDict.lookupOrDefault("nsMaxIter", 50);
const scalar globalTol =
    relaxationDict.lookupOrDefault("globalTol", 1e-3);
const int maxIter =
    relaxationDict.lookupOrDefault("maxIter", 500);



//Convergence of each equation
scalar poissonConv = 1;
scalar nernstPlanckKConv = 1;
scalar nernstPlanckAConv = 1;
scalar nernstPlanckHConv = 1;
scalar nernstPlanckOHConv = 1;
scalar ddConv = 1;
scalar preNSConv = 1;
scalar nsConv = 1;
scalar globalConv = 1;

