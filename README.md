# npfetFOAM
This repository holds the npfetFOAM source code which builds on top of the
OpenFOAM finite-volume toolbox. It is the first ever fully self-consistent
finite-volume solver for semiconductors and flowing electrolytes that includes
calibrated semiconductor, electrolyte, and surface state models. npfetFOAM was
developed to simulate the behavior of the nanopore transistor (nanopore
FET/NPFET), a proposed fully integrated next-generation single-molecule
biosensor.

The main features of the npfetFOAM solver are

- Non-linear Poisson equation for the electric potential
- Drift-Diffusion model for electrons/holes in the semiconductor using a
  Thomas-Caughey mobility model
- Nernst-Planck equations for Na+, Cl-, H+, and buffer species with realistic
  mobilites fitted to literature
- Incompressible Navier-Stokes equation for laminar flow
- Silanol surface state model based on S. Ong et al. (Chemical Physics Letters,
  1992), two population single protonation states
- Semiconductor-Oxide trap states for subthreshold slope degradation
- Surface states and semiconductor model calibrated on IMEC pH-sensing FinFET
  data from M. Gupta et al. (IEEE Sensors Journal, 2019)

![NPFET Illustration](docs/readme_figures/npfet_illustration.png)

![Electric Potential](docs/readme_figures/electric_potential.png)

Note that this is *research code* which means that was the first version
that worked and that means it requires expert knowledge to understand, run, and
modify. The source code here is in no way production-ready and still requires
significant research efforts to explore the discretization, numerics, and
general consistency of the equations.

That said, it is fully open source which means you can freely download and run
it and keep developing it if you like. In order to get started, you need to be
able to do the following things

- Set up an OpenFOAM environment that enables you to compile OpenFOAM based
  code.
- Compile the solvers and utilities contained in the applications directory.
- Run the simulations in this repository and inspect the results using ParaView.

## Getting started with the OpenFOAM toolbox
This work has been tested using OpenFOAM-v1912 but there's no reason it
shouldn't work with newer versions. Plenty of documentation about OpenFOAM can
be found on [openfoam.com](https://openfoam.com/).

To following steps show you how to get the source code of OpenFOAM and how to
keep a debug environment and an optimized environment to develop new code.

- Make an account at the [develop.openfoam.com](https://develop.openfoam.com/).
  This is a GitLab server where the OpenFOAM code is developed.
- Use
  ```
  $ ssh-keygen -t ed25519
  ```
  to make a ssh key pair in your `~/.ssh/` directory
  and upload the public key to the GitLab server on
  [develop.openfoam.com](https://develop.openfoam.com/) by clicking on your user
  icon in the top right and then `Settings > SSH Keys`.
- Clone the source code repository `OpenFOAM-plus` by typing
  ```
  $ git clone git@develop.openfoam.com:Development/OpenFOAM-plus.git OpenFOAM_debug
  ```
  You can also find the ssh clone URL by going to the
  [OpenFOAM repository](https://develop.openfoam.com/Development/OpenFOAM-plus)
  and clicking on the blue `Clone` button in the top right.
  Note that we cloned the repository into a local directory where we plan to do
  the debug build.
- Repeat the above step but this time we aim to make an optimized build
  ```
  $ git clone git@develop.openfoam.com:Development/OpenFOAM-plus.git OpenFOAM_opt
  ```
- For the debug build change the environment variables so that the `wmake`
  build system adds debug symbols by editing the file
  `OpenFOAM_debug/etc/bashrc` and changing the line with the environment
  variable `WM_COMPILE_OPTION` to
  ```
  export WM_COMPILE_OPTION=Debug
  ```
  Note that both `zsh` and `bash` users will use the
  `OpenFOAM_debug/etc/bashrc` file while `csh` users edit
  `OpenFOAM_debug/etc/cshrc`.
- Make aliases to source the optimized and debug OpenFOAM environments by adding
  the following lines to your `.zshrc`, `.bashrc`, or whatever you are using
  ```
  alias ofdebug="source /path/to/OpenFOAM_debug/etc/bashrc"
  alias ofopt="source /path/to/OpenFOAM_opt/etc/bashrc"
  ```
  Source the configuration file or open a new shell to make these changes
  effective.
- Source the debug environment by typing
  ```
  $ ofdebug
  ```
  To test whether this worked, you can type
  ```
  $ foam
  ```
  which should bring you to the `OpenFOAM_debug` directory.
- Follow the steps in the
  [build guide](https://openfoam.com/code/build-guide.php) to compile
  `OpenFOAM_debug`. Don't forget to use multiple cores or the compilation will
  take a lot of time
  ```
  $ export WM_NCOMPPROCS=N
  ```
- Repeat the above two steps for the optimized OpenFOAM environment.
- If you haven't installed the Third-Party tools, you can install `ParaView`
  with your package manager. On Arch Linux the following does the job:
  ```
  $ sudo pacman -S paraview
  ```
  and on Ubuntu you can use
  ```
  $ sudo apt-get install paraview
  ```
- If a new version of OpenFOAM is released, you can update your local copies by
  typing
  ```
  $ ofopt
  $ foam
  $ git pull
  $ ./Allwmake
  ```
  which sources the optimized environment, goes to the `OpenFOAM_opt` directory,
  pulls in new changes from the remote repository, and recompiles. The same can
  be done for the debug environment but note that the local change to
  `OpenFOAM_debug/etc/bashrc` might cause a merge conflict if the remote file
  has also changed this line.


In order to get a feel for how OpenFOAM works, consider doing a few tutorials
from the [tutorial guide](https://openfoam.com/documentation/tutorial-guide/)



## Compiling Solvers and Utilities

This repository contains the `npfetFoam`, a postprocessing utility called
`npfetPostFoam`, and a utility to generate implants called `implant`

To get started, let's clone this repository
```
$ git clone git@gitlab.com:dinoruic/npfet_s3ic2020.git
```
and let's look at the directory structure
```
npfet_s3ic2020
├── applications
│   ├── solvers
│   │   └── npfetFoam
│   │       └── npfetPostFoam
│   └── utilities
│       └── implant
├── docs
│   ├── abstract_S3IC
│   └── readme_figures
├── LICENSE
├── README.md
├── simulation
│   ├── asymmetric_npfet
│   └── mobility_nacl
└── test
    └── npfet_3d
```
The `applications` subdirectory holds the aforementioned solvers and utilities.
`npfetPostFoam` is a subdirectory of `npfetFoam` because they are closely
related. `docs` contains files related to the documentation, where
`docs/abstract_S3IC` contains the abstract submitted to S3IC 2020 and
`docs/readme_figures` holds the figures needed to render this `README.md`. The
`simulation` directory contains all input files and scripts necessary to
generate all plots shown in the S3IC conference publication and presentation.
The `test` directory contains a simple test case to verify the functionality of
the installation.

OpenFOAM uses the `wmake` build system which enables you to easily build
programs that depend on OpenFOAM libraries. To compile a solver or utility, you
source the OpenFOAM environment you would like to use -- let's say the optimized
environment
```
$ ofopt
```
Then you go to the solver's directory and compile a solver such as poissonFoam
```
$ cd npfet_s3ic2020/applications/solvers/npfetFoam
$ wmake
```
Which will make the executable npfetFoam available as long as the optimized
environment is sourced. We can repeat the same steps for the optimized
environment
```
$ ofdebug
$ cd npfet_s3ic2020/applications/solvers/npfetFoam
$ wmake
```
and we will then have the debug executable npfetFoam available as long as
the debug environment is sourced. Thus, we can easily switch between the two
executables by switching our environment with `ofdebug` and `ofopt`.
In order to clean up files and executables created by the compilation of an
application in a certain environment, we can type
```
$ wclean
```
in its directory.

In case you want to compile all solvers and utilities in all subdirectories, you
can use `wmake -a`. Since we will need all executables, type
```
$ cd npfet_s3ic2020/applications
$ wmake -a
```
to compile all of them. Note that `npfetPostFoam` is a subdirectory of
`npfetFoam` because they are closely related.


## Testing the Installation
Solving partial differential equations requires several steps which come with
their own tools. Any simulation needs a mesh, starting values for fields and
boundary conditions, and a call to an appropriate solver. Sometimes other pre-
and postprocessing steps may also be involved. In order to keep track of how to
follow these steps, every example directory contains an `Allrun` shell script
that contains all the necessary steps.  First make sure you compiled
`npfetFoam`, `npfetPostFoam`, and `implant` using
```
$ ofopt
$ cd npfet_s3ic2020/applications
$ wmake -a
```
Then you can try out to run the small test case using
```
$ cd npfet_s3ic2020/test/npfet_3d
$ ./Allrun
```
Do not forget to source the environment `ofopt` of `ofdebug` where the
applications have been compiled.

The simulation will solve a 3d NPFET problem in a few minutes but it omits the
Navier-Stokes equation for brevity. Results can be viewed
using [Paraview](https://www.paraview.org/).
The output has been written in standard OpenFOAM format and you can use the
Paraview initialization routine
```
$ paraFoam
```
which reads in all the fields automatically. Once the window opens click the
green `Apply` button on the left side.

There are many fields defined in various domains in the output but overlapping
domains lead to rendering artifacts. In order to see the domain segments, find
the `Properties` tab in the left side-bar and deselect `internalMesh` in the
`Mesh Regions` list and click the green `Apply` button to hide the global mesh.
To see the cross section, find the `Clip` filter in the top menu bar or in the
Filters menu, click `Apply` (sometimes you need to uncheck the `show Plane`
checkbox to make the clipped structure visible) and rotate the structure a
little bit. Extensive instructions on how to use Paraview can be found on the
[Paraview](https://www.paraview.org/) website and in the tutorials of the
[OpenFOAM](https://www.openfoam.com/) website.

![Paraview NPFET domains](docs/readme_figures/paraview_npfet_domains.png)


## Running Simulations

For performance reasons, you should run the full simulations using optimized
binaries by using
```
$ ofopt
```
and making sure that all binaries above have been built.

### I-V sweeps
Let's now go to a proper simulation directory to run an current-voltage-curve
extraction using a symmetric electrolyte bias. In these simulations the
Navier-Stokes equation is turned off because the electrolyte bias is symmetric
and no electroosmotic flow will exist under these conditions.

To this end, let's go to one of the simulation directories
```
$ cd simulation/asymmetric_npfet/ID_VGS_w30nm
```
The directory contains

- a template input directory `npfet_3d_template`;
- a script `make_instances.sh` to create a sweep of input directories from the
  template;
- a script `run_instances.sh` to start the simulations;
- a script `extract_IV.sh` to scrape the outputs for the current-voltage-curve
  data;
- and `IV.asc` which is the saved output from a previous run.

In order to regenerate the output `IV.asc` you will have to execute
```
$ ./make_instances.sh
$ ./run_instances.sh
```
Then wait until the simulations have concluded and then run
```
$ ./extract_IV.sh
```
Please note that this procedure will start about 10 processes each taking about
2.5GB of memory, so only do this on a workstation or start simulations
individually if you are memory of CPU-core constrained. The sweep will take
about 50 minutes to complete on an Intel Xeon W-2135 CPU with 3.7GHz.

To run your own sweeps sweeps under various symmetric electrolyte bias
conditions, you can simply copy the parent directory, modify the sweep
parameters, or modify constants like device width and channel length in the
template.

Note that you can view each simulation's results by going into the directory
of the simulation instance. For example, from the
template directory `ID_VGS_w30nm`, you can go to
```
$ cd run/VDS_0.10V/VG_0.50V
```
There you will find a `log` file recording the `stdin` and `stderr` output of
the simulation. By typing
```
$ paraFoam
```
you can inspect all fields like the electric potential, the ion
concentrations, the pH, and the electron density in the semiconductor.

Once the sweep has finished you can run the `extract_IV.sh` script from within
the template directory `ID_VGS_w30nm`, to scrape all the resulting semiconductor
currents. This will result in the `IV.asc` file which has already been stored in
the repository from a previous run. Plotting all `IV.asc` files from all sweeps
we obtain the following plot

![IV-curves](docs/readme_figures/IV-curves.png)


### Molecular Translocation Experiments
In this type of simulation we bias difference in the cis and trans
reservoirs of the electrolyte which leads to an electromigration current and an
electroosmotic flow through the nanopore. This means that these simulations
contain the Navier-Stokes equation. In addtion, we place a spherical molecule at
some location in the nanopore to observe how the system changes due to the
field-effect of the molecule and the blockage of the pore current.

To see an example for such a simulation go to
```
$ cd npfet_s3ic2020/simulation/asymmetric_npfet/ID_Pos_charged_molecule
```
The name implies that the simulation produces a plot of `ID`, the drain current
of the nanopore transistor, vs. the position of a charged molecule.
As in the current-voltage sweep, the directory is populated with a template
input directory and scripts to execute a sweep over molecule positions. In order
to execute it, run
```
$ ./make_instance.sh
$ ./run_instances.sh
```
Then wait for the simulation to complete. Note that this will several hours due
to the inclusion of the Navier-Stokes equation. After the simulations have
completed, you can inspect their logs to verify the successful convergence and
then run
```
$ ./extract_IV.sh
```
to regenerate `IV.asc`. The contents in of `IV.asc` for the charged molecule and 
for an uncharged molecule can be plotted as a current vs. molecule position as
the expected signal profile in the nanopore transistor current output

![Signal Profile](docs/readme_figures/current_signal.png)


### Plotting Changes in Electric Potential
In order to understand how exactly the presence of the molecule impacts the
electric potential, we need to calculate the difference of the electric
potential with and without the molecule. To this end, we copy the reference
solution from `ID_Pos_reference` over to the current directory. Due to the
naming conflict, we also have to rename it. Both can be achieved by executing
the simple script
```
$ ./cp_pot_ref.sh
```
The difference can be formed using the `Python Calculator` filter in Paraview
and it looks like this:

![Potential Difference](docs/readme_figures/potential_diff_10e.png)


### Sensitivities
You can find several sweeps over bulk pH in the subdirectories of
```
$ cd npfet_s3ic2020/simulation/asymmetric_npfet/sensitivity
```
One for each bulk salt concentration `c_1mM`, `c_10mM`, and `c_100mM`. Each of
them contain a script `sensitivity.sh` that can be run to gather the outputs of
each of the pH sweeps. Plotting all these results together demonstrates the
sensitivity of the nanopore transistor in various conditions

![Sensitivity](docs/readme_figures/sensitivity.png)


### Rendering 3D Output
In many cases you will need to render output in 3D or view slices of fields
through cross-sections of the device. Paraview will prove really useful in this
case which allows you to inspect details fields during development of code,
quickly apply complicated filters to data, and also render high-quality figures
which may even be ray-traced at the click of a button.

All outputs that have been generated with Paraview for this work have been saved
in the directory
```
$ cd npfet_s3ic2020/simulation/asymmetric_npfet/Output
```
The directory also contains `*.pvsm` files which contain the state of Paraview
when the figures were created. To open one of them, you will have to start
Paraview
```
$ paraview
```
then click on the `File` menu in the top left, select `Load State`, and choose
the `.pvsm` file you are interested in.

Unfortunately Paraview uses absolute paths to identify the input data, so after
you selected your `.pvsm` file, there will be a dialog where you have to select
`Choose File Names` and then identify which simulation files in the
`npfet_s3ic_2020/simulations` in your directory hierarchy correspond to the ones
assumed in the `.pvsm` file.


## Electrolyte Mobility Model
The Nernst-Planck equations for Na+ and Cl- assume the Einstein relation holds
between the mobility and diffusion constants which does not necessarily hold for
high concentrations but is a simplification we used to get started.

For the mobilities of Na+ and Cl-, we assembled results from literature and
fitted a model. The data and fitting procedure is located in
`npfet_s3ic2020/simulation/mobility_nacl`. If you have `python3` with the modules
`pandas`, `matplotlib`, and `scipy` installed, you can redo the model extraction
by typing
```
$ cd npfet_s3ic2020/simulation/mobility_nacl
$ make
```
which will print the model and the fitted parameters as well as display a window
with the fitted graph and experimental data. You can find these numbers in the
input files of of npfetFoam solver.


## Building Docs
This repository contains the abstract of the submission to the Single-Molecule
Sensors and NanoSystems International Conference 2020. The figures can be
regenerated using a LaTeX which includes `tikz` and `pgfplots` packages. You
should install [TeX Live](http://tug.org/texlive) using your Linux's package
manager to make sure you have all the dependencies, e.g. on Ubuntu
```
$ sudo apt-get install texlive texlive-science
```
Also make sure that you have the ImageMagick `convert` utility installed on your
system to convert the resulting PDF to a JPG if you need it.

To build a figure, e.g. the 3D render of the NPFET with the text overlay and
caption, do
```
$ cd npfet_s3ic2020/docs/abstract_S3IC/figures/pot_diff
$ make
```
Note that you won't need to rerun the simulations for that because outputs like
the rendered potential difference cross-sections of the NPFET have already been
saved in `npfet_s3ic2020/simulation/asymmetric_npfet/Output`. The resulting
plot of that was used for the abstract contains annotations and a caption:

![Potential Differences](docs/readme_figures/3_pot_diff.jpg)



## License
npfetFoam solver and utilities Copyright (C) 2020 Dino Ruic

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.
